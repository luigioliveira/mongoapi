const express = require('express');
const bodyParser = require('body-parser');
const mongodb = require('mongodb');
const usersRouter = require('./Router/users')
const contactsRouter = require('./Router/contacts')
const mongoose = require('mongoose');
const url = 'mongodb+srv://root:root@tads.n8sdk.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';
const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true
    };

const port = 3000;

let app = express();

mongoose.connect(url, options)
.then(console.log('connecting'))
.catch(err => console.log(`error: ${err}`));
mongoose.connection.on('error',(err)=>{
console.log("Erro:" + err);
});
mongoose.connection.on('disconnected',()=>{
console.log("Aplicação desconectada");
});

app.use(bodyParser.json());
app.use('/usuarios',usersRouter);
app.use('/contatos',contactsRouter);

app.listen(port, () => {
    console.log("==============================================================");
    console.log("Projeto API Multas por Luigi Oliveira iniciado!");
    console.log("Projeto executando na porta: " + port);
    console.log("==============================================================");
})