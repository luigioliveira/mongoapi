const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    return res.send('MÉTODO GET DO USER')
})

router.post('/', (req, res) => {
    return res.send('MÉTODO POST DO USER')
})

module.exports = router;